﻿import { Component } from '@angular/core';
import { FormControl, FormGroupDirective, NgForm, Validators } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import { MatSuffix } from '@angular/material';

export class MyErrorStateMatcher implements ErrorStateMatcher {
    isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
        const isSubmitted = form && form.submitted;
        return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
    }
}

@Component({
    selector: 'login',
    templateUrl: 'login.component.html',
    styleUrls: ['login.component.css'],
})
export class LoginComponent {
    title: string;

    constructor() {
        this.title = 'Авторизация';
    }

    // the form control are necessary for login form validation
    UsernameFormControl = new FormControl('', [Validators.required]);
    PasswordFormControl = new FormControl('', [Validators.required]);

    // return true if the form invalid
    isInvalidForm() {
        return this.PasswordFormControl.hasError('required') || this.UsernameFormControl.hasError('required');
    }
    matcher = new MyErrorStateMatcher();
}

