﻿import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

/**
 * @title Card with multiple sections
 */
@Component({
    selector: 'cardstaff',
    templateUrl: 'cardstaff.component.html',
    styleUrls: ['cardstaff.component.css'],
})
export class CardStaffComponent implements OnInit{

    constructor(private matDialogRef: MatDialogRef<CardStaffComponent>, @Inject(MAT_DIALOG_DATA) public data: any) {
    }

    ngOnInit() {

    }

    close() {
        this.matDialogRef.close();
    }
}
