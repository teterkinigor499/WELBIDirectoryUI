﻿import { Component, ViewChild } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatDialog } from '@angular/material';

import { CardStaffComponent } from '../cardstaff/cardstaff.component';

@Component({
    selector: 'staff',
    templateUrl: './staff.component.html',
    styleUrls: ['./staff.component.css']
})
export class StaffComponent {
    constructor(public dialog: MatDialog) { }

    displayedColumns = ['name', 'position', 'workPhone'];
    dataSource = new MatTableDataSource(ELEMENT_DATA);
    /*
    @ViewChild(MatPaginator) public paginator: MatPaginator;

    ngAfterViewInit() {
        this.dataSource.paginator = this.paginator;
    }
    */

    applyFilter(filterValue: string) {
        filterValue = filterValue.trim();
        filterValue = filterValue.toLowerCase();
        this.dataSource.filter = filterValue;
    }

    openDialog(id: number) {
        this.dialog.open(CardStaffComponent, {
            // реализовать получение информации из базы по api
        });
    }
}

export interface Element {
    photo: number;
    name: string;
    position: string;
    email: string;
    workPhone: number;
}

const ELEMENT_DATA: Element[] = [
    { photo: 2, name: 'Иванов Иван Иванович', position: 'Начальник', email: 'ivanov@mail.ru', workPhone: 3463 },
    { photo: 3, name: 'Сергеев Сергей Сергеевич', position: 'Руководитель', email: 'sergeev@bk.ru', workPhone: 4321 },
    { photo: 4, name: 'Евгеньев Евгений Евгеньевич', position: 'Менеджер', email: 'evgenev@rambler.ru', workPhone: 5843 },
    { photo: 5, name: 'Михаилов Михаил Михайлович', position: 'Администратор', email: 'mixail3443@yandex.ru', workPhone: 3275 },
    { photo: 6, name: 'Петров Пётр Петрович', position: 'Тренер', email: 'petrovP@mail.ru', workPhone: 5468 },
    { photo: 7, name: 'Филонов Филон Факториалович', position: 'Руководитель', email: 'philon@google.com', workPhone: 3646 },
    { photo: 8, name: 'Тарасов Тарас Тарасович', position: 'Администратор', email: 'tarasov@yandex.ru', workPhone: 3275 },
    { photo: 9, name: 'Константинов Константин Константинович', position: 'Тренер', email: 'kostik1990@mail.ru', workPhone: 5468 },
    { photo: 10, name: 'Чижиков Чижик Чижикович', position: 'Руководитель', email: 'chizicov@google.com', workPhone: 3646 },
    { photo: 11, name: 'Фралова Ольга Михайловна', position: 'Начальник', email: 'frolga@google.com', workPhone: 3646 },
    { photo: 12, name: 'Тетеркин Игорь Александрович', position: 'Заместитель', email: 'teterkinigor499@gmail.com', workPhone: 1227 },
];
