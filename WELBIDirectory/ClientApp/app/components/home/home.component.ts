import { Component, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { CardStaffComponent } from '../cardstaff/cardstaff.component';

@Component({
    selector: 'home',
    templateUrl: 'home.component.html',
    styleUrls: ['home.component.css'],
})
export class HomeComponent {
    constructor(public dialog: MatDialog) { }

    openDialog() {
        const dialogRef = this.dialog.open(CardStaffComponent, {
        });
    }
}

