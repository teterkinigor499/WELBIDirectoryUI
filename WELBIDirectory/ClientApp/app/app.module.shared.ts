import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

// import component use current project
import { AppComponent } from './components/app/app.component';
import { NavMenuComponent } from './components/navmenu/navmenu.component';
import { HomeComponent } from './components/home/home.component';
import { DepartmentsComponent } from './components/departments/departments.component';
import { StaffComponent } from './components/staff/staff.component';
import { CardStaffComponent } from './components/cardstaff/cardstaff.component';
import { CounterComponent } from './components/counter/counter.component';
import { AboutComponent } from './components/about/about.component';
import { HandBookComponent } from './components/handbook/handbook.component';
import { LoginComponent } from './components/login/login.component';

// import Angular Material component
import {
    MatButtonModule,
    MatCheckboxModule,
    MatInputModule,
    MatListModule,
    MatTableModule,
    MatPaginatorModule,
    MatIconModule,
    MatMenuModule,
    MatExpansionModule,
    MatDialogModule,
    MatCardModule,
    MatGridListModule,
    MatFormFieldModule,
    MatTooltipModule
} from '@angular/material';

@NgModule({
    declarations: [
        AppComponent,
        NavMenuComponent,
        HomeComponent,
        DepartmentsComponent,
        StaffComponent,
        CounterComponent,
        AboutComponent,
        HandBookComponent,
        CardStaffComponent, 
        LoginComponent
    ],
    imports: [
        CommonModule,
        HttpModule,
        BrowserAnimationsModule,
        ReactiveFormsModule,
        FormsModule,

        // Angular Material component
        MatButtonModule,
        MatCheckboxModule,
        MatInputModule,
        MatListModule,
        MatTableModule,
        MatPaginatorModule,
        MatIconModule,
        MatMenuModule,
        MatExpansionModule,
        MatDialogModule,
        MatCardModule,
        MatGridListModule,
        MatFormFieldModule,
        MatTooltipModule,

        RouterModule.forRoot([
            { path: '', redirectTo: 'home', pathMatch: 'full' },
            { path: 'home', component: HomeComponent },
            { path: 'login', component: LoginComponent },
            { path: 'counter', component: CounterComponent },
            { path: 'handbook', component: HandBookComponent },
            { path: 'about', component: AboutComponent },
            { path: '**', redirectTo: 'home' }
        ])
    ],
    entryComponents: [
        CardStaffComponent
    ]
})
export class AppModuleShared {
}
